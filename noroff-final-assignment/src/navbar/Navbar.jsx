import { NavLink } from "react-router-dom"
import { useUser } from "../context/UserContext"
import './NavbarStyle.css';


const Navbar = () => {
    
    const {user} = useUser()    
    return(
        <nav className="Navbar-Color">
            {user !== null &&
            <ul >
                <li className="Navbar-Li">
                    <NavLink to ="/translate">Translate</NavLink>
                </li>
                <li className="Navbar-Li">
                    <NavLink to="/profile">Profile</NavLink>
                </li>
            </ul>
            }
            
        </nav>
    )
}
export default Navbar