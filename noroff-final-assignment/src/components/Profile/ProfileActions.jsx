import { Link } from "react-router-dom"
import { translateClearHistory } from "../../api/translate"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import '../../index.css'

const ProfileActions = () => {
    
    const {user, setUser} = useUser()

    const handleLogoutClick= () => {
        if(window.confirm('Are you sure?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }
    
const handleClearHistoryClick = async () =>{
    if(!window.confirm('Are you sure?\nThis can not be undone!!')){        
        return
    }
    const [clearError] = await translateClearHistory(user.id)
    
if(clearError !== null){
    return
}
    const updatedUser={
        ...user,
        translations:[]
    }
    setUser(updatedUser)
    storageSave(STORAGE_KEY_USER,updatedUser)
}

    return(
    <>
<nav class="nav justify-content-end">
  <button onClick={ handleClearHistoryClick}>clear history</button>
  <button onClick={ handleLogoutClick }>logout</button>
</nav>
    </>
    )
}

export default ProfileActions