import ProfileTranslateHistoryItem from "./ProfileTranslateHistoryItem"
import '../../index.css'

const ProfileTranslateHistory = ({translates}) => {
    
    const translateList = translates.map((translate, index) => <ProfileTranslateHistoryItem key={ index + '-' + translate } translateItem={translate}/>)
    
    return(
    <section>
        <h4 className="AlignCenter">Your Translate History</h4>

        {translateList.length === 0 && <p className="AlignCenter">You have no previous translates</p>}

        <ul> {translateList} </ul>
    </section>
    )
}

export default ProfileTranslateHistory