import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import {loginUser} from '../../api/user'
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

// Won't be recreated when re-rendered. optimalized when outside
const usernameConfig = {
  required: true,
  minLength: 3,
};

const LoginForm = () => {
  //hooks
  const {register,handleSubmit,formState: { errors } } = useForm();
  const {user,setUser} = useUser()
  const navigate = useNavigate()

  //local state
  const [loading, setLoading] = useState(false)
  const [apiError, setApiError] = useState(null)
  

  //side effects
  useEffect(() => {
    if(user !== null){
      navigate('/profile')
    }
  }, [user, navigate])

  //event handler
  const onSubmit = async ({username}) => {
    setLoading(true)
    const [error, userResponse] = await loginUser(username)
    if(error !== null){
      setApiError(error)
    }
    if(userResponse !== null){
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
    setLoading(false)

  };

  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }

    let err = "";

    if (errors.username.type === "required") {
      err = "Username is Required";
    }

    if (errors.username.type === "minLength") {
      err = "Username is to short, min length is 3 characters";
    }
    
    return <span>{err}</span>;
  })();

  return (
    <>
      <h4 className="AlignCenter">What's your username?</h4>
      <form onSubmit={handleSubmit(onSubmit)} className="AlignCenter">

          <label htmlFor="username"></label>
          <input
            type="text"
            placeholder="username"
            {...register("username", usernameConfig)}
          />
          {errorMessage}

        <button type="submit" disabled = {loading}>Continue</button>
        {loading && <p>Logging in ...</p>}
        {apiError && <p>{apiError}</p>} 
      </form>
    </>
  );
};

export default LoginForm;
