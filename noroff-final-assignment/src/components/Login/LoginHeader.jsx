import '../../index.css'

const LoginHeader = () =>{
    return (
        <>  
   
        <header className='Header'>
            <h1 className="AlignCenter">Lost in Translation</h1> 
            <p className='AlignCenter'><img src="Logo.png" alt="" className='float-left' width={100}/></p>   
        </header>
        <p className><h3 className="AlignCenter">Please log in</h3></p>
        </>        
    )
}
export default LoginHeader