import TranslateTextToSignItem from "./TranslateTextToSignItem"

const TranslateTextToSign = ({text}) => {
    
    const translatedText = text.split("").map(((char, index) => <TranslateTextToSignItem key={index +'-'+char} charToTranslate={char}/>))    
    return(
        <p className="AlignCenter">
            {translatedText}
        </p>
    )
}
export default TranslateTextToSign