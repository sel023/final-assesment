import { useForm } from "react-hook-form"

const TranslateForm = ({onTranslate}) => {

    const {register, handleSubmit} = useForm()

    const onSubmit = ({translateText}) =>{onTranslate(translateText)}

    return(
        <>
        <form onSubmit={handleSubmit(onSubmit)} className="AlignCenter">
                <label htmlFor="translate-text">Text to be translated:</label>
                <p>
                    <input type="text" {...register('translateText')} placeholder="hello" />
                </p>
            <button type="submit">Translate</button>
        </form>

        </>
    )

}
export default TranslateForm