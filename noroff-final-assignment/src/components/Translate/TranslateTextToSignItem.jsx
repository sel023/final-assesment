function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
  }

const TranslateTextToSignItem = ({charToTranslate}) =>{
    if(charToTranslate === " "){
        return <p></p>
    }
    if(!isLetter(charToTranslate)){
        return <p>{charToTranslate}</p>
    }
    return <img src={"img/"+ charToTranslate + ".png"} alt="" width="50" />
}
export default TranslateTextToSignItem