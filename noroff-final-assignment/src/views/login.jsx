import LoginForm from "../components/Login/LoginForm"
import LoginHeader from "../components/Login/LoginHeader"

const Login = () =>{
    return(
        <>
        <LoginHeader/>        
        <LoginForm></LoginForm>        
        </>
    )
}

export default Login