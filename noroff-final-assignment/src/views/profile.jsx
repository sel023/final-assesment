import { useEffect } from "react"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslateHistory from "../components/Profile/ProfileTranslateHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"

const Profile = () =>{

    const {user} = useUser()

    useEffect(() =>{
        
    },[])

    return(
        <>
        <ProfileHeader username={user.username}/>
        <ProfileActions />
        <ProfileTranslateHistory translates = {user.translations}/>
        </>
    )
}

export default withAuth(Profile)