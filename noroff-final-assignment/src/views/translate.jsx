import { useState } from "react"
import { translateAdd } from "../api/translate"
import TranslateForm from "../components/Translate/TranslateForm"
import TranslateHeader from "../components/Translate/TranslateHeader"
import TranslateTextToSign from "../components/Translate/TranslateTextToSign"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"



const Translate = () =>{

    const [textToBeTranslated, setTextToBeTranslated] = useState(null)
    const [apiOrTranslateError, setError] = useState(null)
    const {user, setUser} = useUser()

    const handleTranslateClicked = async (text) =>{
        //check text, display translation
        //send history to api
        const textToApi = text.trim()
        const [error, updatedUser] = await translateAdd(user,textToApi)
        if(error !== null){
            setError(error)
            return
        }
            
        setTextToBeTranslated(textToApi)
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
        

    }

    return(
        <>
        <TranslateHeader />
        <section id ="translate-text">
            <TranslateForm onTranslate={handleTranslateClicked}/>
        </section>
        <section id = "translated-text">
        {textToBeTranslated && <TranslateTextToSign text={textToBeTranslated}/>}
        {apiOrTranslateError && <p>{apiOrTranslateError}</p>}
        </section>        
        </>
    )
}
//Protecting the app from unwanted activity
export default withAuth(Translate)