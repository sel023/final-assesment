import {createHeaders} from './index'

const apiUrl = process.env.REACT_APP_API_URL

export const translateAdd = async (user, translateText) =>
{
    try {
        const response = await fetch(`${apiUrl}/${user.id}`,
            {
                method : 'PATCH',
                headers: createHeaders(),
                body: JSON.stringify({
                    username: user.username,
                    translations: [...user.translations, translateText]
                })
            })

            if(!response.ok){
                throw new Error('Could not update user with new translate history')
            }

            const result = await response.json()
            return [null,result]
    } catch (error) {
        return [error.message,null]
    }
}

export const translateClearHistory = async (userId) =>{
    try {
        const response = await fetch(`${apiUrl}/${userId}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations : []
            })
        })
        const result = await response.json()
        if(!response.ok){
            throw new Error('could not update orders')
        }
        return[null, result]
    } catch (error) {
        return [error.message,null]
    }
}