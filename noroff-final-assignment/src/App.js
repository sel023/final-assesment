import './App.css';
import {BrowserRouter, Routes,Route} from 'react-router-dom'
import Login from './views/Login';
import Translate from './views/Translate';
import Profile from './views/Profile';
import Navbar from './navbar/Navbar';
import Footer from './footer/Footer';

function App() {
  
  return (
    <BrowserRouter>
    <div className="App">
      <Navbar/>
      <Routes>
        <Route path = "/" element = {<Login/>} />
        <Route path = "/translate" element = {<Translate/>} />
        <Route path = "/profile" element = {<Profile/>} />
        <Route path = "/login" element = {<Login/>} />
      </Routes >
      <Footer/>
    </div>
    </BrowserRouter>
  );
}

export default App;
